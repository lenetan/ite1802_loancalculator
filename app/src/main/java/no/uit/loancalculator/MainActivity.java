package no.uit.loancalculator;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONArray;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private Loan loan;
    private int year = 0;
    private double termPayment;
    private double interestPaid;
    private double installment;
    private SeekBar seekBar_Amount;
    private SeekBar seekBar_Interest;
    private SeekBar seekBar_Term;
    private TextView show_Amount;
    private TextView show_Interest;
    private TextView show_Term;
    private ListView listView_result;
    static ArrayList<String> ar = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            setTheme(R.style.DarkTheme);
        } else {
            setTheme(R.style.LightTheme);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupUIViews();
        setupListener();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    private void setupUIViews() {
        seekBar_Amount = findViewById(R.id.seekBar_Amount);
        seekBar_Interest = findViewById(R.id.seekBar_Interest);
        seekBar_Term = findViewById(R.id.seekBar_Time);
        show_Amount = findViewById(R.id.show_Amount);
        show_Interest = findViewById(R.id.show_Interest);
        show_Term = findViewById(R.id.show_Time);
        listView_result = findViewById(R.id.listView_result);

        String txt_amount = ("0");
        String txt_interest = ("0");
        String txt_time = ("0");
        show_Amount.setText(txt_amount);
        show_Interest.setText(txt_interest);
        show_Term.setText(txt_time);
        seekBar_Amount.setProgress(0);
        seekBar_Interest.setProgress(0);
        seekBar_Term.setProgress(0);
    }

    private void setupListener() {
        seekBar_Amount.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                this.progress = progress;

                String amount = ((progress * 100000) + "");
                show_Amount.setText(amount);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //ignore
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //ignore
            }
        });

        seekBar_Interest.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                this.progress = progress;
                String interest = ((progress) + "");
                show_Interest.setText(interest);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //ignore
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //ignore
            }
        });

        seekBar_Term.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                this.progress = progress;
                String term = ((progress) + "");
                show_Term.setText(term);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //ignore
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //ignore
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("seekbar_Amount", seekBar_Amount.getProgress());
        outState.putInt("seekbar_Interest", seekBar_Interest.getProgress());
        outState.putInt("seekbar_Time", seekBar_Term.getProgress());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        seekBar_Amount.setProgress(savedInstanceState.getInt("seekbar_Amount"));
        seekBar_Interest.setProgress(savedInstanceState.getInt("seekbar_Interest"));
        seekBar_Term.setProgress(savedInstanceState.getInt("seekbar_Time"));
        getLoanPayments();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_calculate:
                getLoanPayments();
                return true;

            case R.id.btn_settings:
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent);
                return true;

            case R.id.btn_export:
                exportToJSON();
                return true;

            case R.id.btn_help:
                int theme;

                if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
                    theme = R.style.DarkAlertDialogTheme;
                } else {
                    theme = R.style.AlertDialogTheme;
                }

                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this, theme).create();
                alertDialog.setTitle(this.getString(R.string.btn_help));
                alertDialog.setMessage(this.getString(R.string.txt_helpDialog));
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.txt_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
                return true;

            case R.id.btn_exit:
                finish();
                System.exit(0);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    private void getLoanPayments() {
        year = 0;
        double chosenAmount = Integer.parseInt(show_Amount.getText().toString());
        double chosenInterest = Integer.parseInt(show_Interest.getText().toString());
        int chosenTerm = Integer.parseInt(show_Term.getText().toString());
        double remainingDebt = chosenAmount;

        loan = new Loan(chosenAmount, chosenInterest, chosenTerm, remainingDebt);

        ar.clear();
        while (remainingDebt > 0) {
            remainingDebt = calculateValues(remainingDebt);
            useListAdapter(year, termPayment, interestPaid, installment, remainingDebt);
        }
    }

    private double calculateValues(double remainingDebt) {
        year++;
        termPayment = (double) Math.round(loan.getTermPayment() * 100d) / 100d;
        interestPaid = (double) Math.round(loan.getInterestPaid() * 100d) / 100d;
        installment = (double) Math.round(loan.getInstallment() * 100d) / 100d;
        remainingDebt = (double) Math.round(loan.getRemainingDebt() * 100d) / 100d;
        return remainingDebt;
    }

    private void useListAdapter(int year, double termPayment, double interest, double installment, double remainingDebt) {
        String str_year = String.valueOf(year);
        String str_termPayment = String.valueOf(termPayment);
        String str_interest = String.valueOf(interest);
        String str_installment = String.valueOf(installment);
        String str_remainingDebt = String.valueOf(remainingDebt);

        String save = str_year + " " + str_termPayment + " " + str_interest + " " + str_installment + " " + str_remainingDebt;
        ar.add(save);

        String[] array = new String[ar.size()];
        int i = 0;
        for (String s : ar) {
            array[i++] = s;
        }

        ListAdapter listAdapter = new MyAdapter(this, array);
        listView_result.setAdapter(listAdapter);
    }

    /* EXPORTS VALUES TO FILE VALUES.JSON TO THE DOWNLOADS DIRECTORY */
    @SuppressLint("NewApi")
    public void exportToJSON() {
        JSONArray jsArray = new JSONArray(ar);

        File documentFolder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        final File file = new File(documentFolder.getAbsolutePath(), "values.json");

        if (Build.VERSION.SDK_INT >= 23) {
            checkForPermission();
        }

        try (FileWriter fileWriter = new FileWriter(file, false)) {
            fileWriter.write(jsArray.toString());
            fileWriter.write("\n\n");

            fileWriter.close();
            Toast.makeText(this, this.getString(R.string.txt_saved), Toast.LENGTH_LONG).show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void checkForPermission() {
        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }
}