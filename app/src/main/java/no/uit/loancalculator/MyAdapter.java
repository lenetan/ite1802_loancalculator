package no.uit.loancalculator;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

class MyAdapter extends ArrayAdapter<String> {
    private TextView tv_year;
    private TextView tv_termPayment;
    private TextView tv_interest;
    private TextView tv_installment;
    private TextView tv_remainingDebt;
    private String[] value;
    private String year;
    private String termPayment;
    private String interest;
    private String installment;
    private String remainingDebt;


    MyAdapter(Context context, String[] values) {
        super(context, R.layout.values_layout, values);
        this.value = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View myView = layoutInflater.inflate(R.layout.values_layout, parent, false);

        for (int i = 0; i < value.length; i++) {
            String yearlyInformation = value[position].trim();

            String[] eachValue = yearlyInformation.split(" ");
            for (int y = 0; y < eachValue.length; y++) {
                year = eachValue[0];
                termPayment = eachValue[1];
                interest = eachValue[2];
                installment = eachValue[3];
                remainingDebt = eachValue[4];
            }
        }
        setupUIViews(myView);

        tv_year.setText(year);
        tv_termPayment.setText(termPayment);
        tv_interest.setText(interest);
        tv_installment.setText(installment);
        tv_remainingDebt.setText(remainingDebt);

        return myView;
    }

    private void setupUIViews(View myView) {
        tv_year = myView.findViewById(R.id.tv_year);
        tv_termPayment = myView.findViewById(R.id.tv_termPayment);
        tv_interest = myView.findViewById(R.id.tv_interest);
        tv_installment = myView.findViewById(R.id.tv_installment);
        tv_remainingDebt = myView.findViewById(R.id.tv_remainingDebt);
    }
}
