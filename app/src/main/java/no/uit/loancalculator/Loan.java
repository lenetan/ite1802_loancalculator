package no.uit.loancalculator;

public class Loan {
    private int terms;
    private double amount;
    private double termPayment;
    private double interestPaid;
    private double interest;
    private double interestInDec;
    private double installment;
    private double remainingDebt;

    Loan() {
        this(0, 0, 0, 0);
    }

    public Loan(double amount, double interest, int terms, double remainingDebt) {
        this.amount = amount;
        this.interest = interest;
        this.terms = terms;
        this.remainingDebt = remainingDebt;

        interestInDec = interest / 100;
    }

    public double getAmount() {
        return amount;
    }

    public double getInterestPaid() {
        interestPaid = remainingDebt * interestInDec;
        return interestPaid;
    }

    public double getTermPayment() {
        termPayment = amount * (Math.pow((1 + interestInDec), terms) * interestInDec) /
                (Math.pow((1 + interestInDec), terms) - 1);
        return termPayment;
    }

    public double getInstallment() {
        installment = termPayment - interestPaid;
        return installment;
    }

    public double getRemainingDebt() {
        remainingDebt = remainingDebt - getInstallment();
        return remainingDebt;
    }

    public int getTerms() {
        return terms;
    }

    public void setInterestPaid(double interestPaid) {
        this.interestPaid = interestPaid;
    }

    public double getInterest() {
        return interest;
    }

    public void setTerms(int terms) {
        this.terms = terms;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setTermPayment(double termPayment) {
        this.termPayment = termPayment;
    }

    public void setInterest(double interest) {
        this.interest = interest;
    }

    public void setInterestInDec(double interestInDec) {
        this.interestInDec = interestInDec;
    }

    public void setInstallment(double installment) {
        this.installment = installment;
    }

    public void setRemainingDebt(double remainingDebt) {
        this.remainingDebt = remainingDebt;
    }
}