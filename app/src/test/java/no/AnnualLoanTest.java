package no;

import org.junit.Before;
import org.junit.Test;

import no.uit.loancalculator.Loan;

import static org.junit.Assert.assertEquals;

public class AnnualLoanTest {
    Loan loan1;
    Loan loan2;

    @Before
    public void before() {
        loan1 = new Loan(200000, 3.6, 12, 200000);
        loan2 = new Loan(200000, 3.6, 12, 186381.22);
    }

    @Test
    public void getTermPayment() {
        assertEquals(20818.78, loan1.getTermPayment(), 0.5);
        assertEquals(20818.78, loan2.getTermPayment(), 0.5);
    }

    @Test
    public void getInterestPaid() {
        assertEquals(7200, loan1.getInterestPaid(), 0.5);
        assertEquals(6709.72, loan2.getInterestPaid(), 0.5);
    }

    @Test
    public void getInnstallment() {
        assertEquals(0, loan1.getInstallment(), 0.5);
        assertEquals(0, loan2.getInstallment(), 0.5);
    }

    @Test
    public void getRemainingDebt() {
        assertEquals(200000, loan1.getRemainingDebt(), 0.5);
        assertEquals(186381.22, loan2.getRemainingDebt(), 0.5);
    }

    @Test
    public void getTotalPayment() {
        assertEquals(200000, loan1.getAmount(), 0.5);
        assertEquals(200000, loan2.getAmount(), 0.5);
    }
}